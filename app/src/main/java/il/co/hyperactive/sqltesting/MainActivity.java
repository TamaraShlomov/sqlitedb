package il.co.hyperactive.sqltesting;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText nameEditText, familyEditText;
    SQLiteDatabase database;
    private static String DATABASE_NAME = "NamesDataBase";
    private static String TABLE_NAME = "tbl_names";
    private static String TABLE_ROW_NAME = "name";
    private static String TABLE_ROW_FAMILY = "family";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database =  openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        String CreateTableCommand = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
        TABLE_ROW_NAME + " TEXT, " +TABLE_ROW_FAMILY+ " TEXT);";
        database.execSQL(CreateTableCommand);
        initViews();
        findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameEditText.getText().toString();
                String family = familyEditText.getText().toString();
                ContentValues values = new ContentValues();
                values.put(TABLE_ROW_NAME, name);
                values.put(TABLE_ROW_FAMILY, family);
                database.insert(TABLE_NAME, null, values);
            }
        });
        findViewById(R.id.buttonShow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor myCursor = database.query(TABLE_NAME, null, null, null, null, null, null);

                if( myCursor.moveToFirst())
                {
                    int nameRowIndex = myCursor.getColumnIndex(TABLE_ROW_NAME);
                    int familyRowIndex = myCursor.getColumnIndex(TABLE_ROW_FAMILY);
                    do {


                        String name = myCursor.getString(nameRowIndex);
                        String family = myCursor.getString(familyRowIndex);
                        Log.i("Name and Family =", name + " " + family);
                    }
                    while(myCursor.moveToNext());
                    myCursor.close();
                }
            }
        });

    }

    private void initViews()
    {
        nameEditText = (EditText)findViewById(R.id.editTextName);
        familyEditText = (EditText)findViewById(R.id.editTextFamily);
    }
}
